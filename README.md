# Installation Guide
1. Clone this repo.
```
git clone https://gitlab.com/iqrar99/full-room.git
```

2. Create virtual environment by using `venv` inside the repository.

On MacOs and Linux:
```
python3 -m venv env
```
On Windows:
```
python -m venv env
```
3. Activate the environment.

On MacOS and Linux:
```bash
source env/bin/activate
```

On Windows:
```
.\env\Scripts\activate
```

4. Install all the packages.
```
pip install -r requirements.txt
```

5. Run the program.
```
python manage.py runserver
```