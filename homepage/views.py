from django.shortcuts import render

def homepage(request):
    return render(request, 'homepage.html')

def education(request):
    return render(request, 'education.html')
